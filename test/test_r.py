#!/opt/anaconda/bin/python

import unittest
from cookiecutter.main import cookiecutter
import os
import json
import subprocess
import shutil

def launch_process(args):
 
    process = subprocess.Popen(args,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
   

    stdout = process.communicate()[0]

    if process.returncode != 0 :

        message='Error executing {} ({})'.format(' '.join(args), process.returncode)
    
    else: 
    
        message='Execution completed ({})'.format(process.returncode)

    return message, stdout

class RTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        with open('artifacts/r.json') as json_file:
            self.data = json.load(json_file)
        
        self.app_path = cookiecutter(template='../../cookiecutter-ewf-r', 
             extra_context=self.data, 
             no_input=True)
        
    def test_env_R(self):
        
        outcome = os.path.exists('/opt/anaconda/envs/{}/bin/R'.format(str(self.data['environment'])))
        self.assertEqual(outcome, True)
      
    def test_application_xml(self):
        
        outcome = os.path.exists(os.path.join(self.app_path, 'src', 'main', 'app-resources', 'application.xml'))
        self.assertEqual(outcome, True)
      
    def test_hash_bang(self):
        nodes = ['node_A', 'node_B'] 
   
        for node in nodes: 
            with open(os.path.join(self.app_path, 'src', 'main', 'app-resources', node, 'run')) as f:
                first_line = f.readline()       
    
            self.assertEqual(first_line, '#!/opt/anaconda/envs/{}/bin/Rscript --vanilla --slave --quiet\n'.format(str(self.data['environment'])))
 
    def test_env_yml(self):
        
        outcome = os.path.exists(os.path.join(self.app_path, 'src', 'main', 'app-resources', 'dependencies', 'R', 'environment.yml'))
        self.assertEqual(outcome, True)

    @classmethod
    def tearDownClass(self):
        
        command = 'sudo conda remove --name {} --all --yes'.format(str(self.data['environment']))
        launch_process(command.split(' '))

        shutil.rmtree(self.app_path)

if __name__ == '__main__':
    unittest.main()

    def test_hash_bang(self):
        nodes = ['node_A', 'node_B'] 
   
        for node in nodes: 
            with open(os.path.join(self.app_path, 'src', 'main', 'app-resources', node, 'run')) as f:
                first_line = f.readline()       
    
            self.assertEqual(first_line, '#!/opt/anaconda/envs/{}/bin/Rscript --vanilla --slave --quiet\n'.format(str(self.data['environment'])))
 
    def test_env_yml(self):
        
        outcome = os.path.exists(os.path.join(self.app_path, 'src', 'main', 'app-resources', 'dependencies', 'R', 'environment.yml'))
        self.assertEqual(outcome, True)

    @classmethod
    def tearDownClass(self):
        
        command = 'sudo conda remove --name {} --all --yes'.format(str(self.data['environment']))
        launch_process(command.split(' '))

        shutil.rmtree(self.app_path)

if __name__ == '__main__':
    unittest.main()

